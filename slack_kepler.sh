#!/bin/sh

usage() {
cat <<EOF
usage $0 options
OPTIONS:
    -h  Show this message
    -t  Set TESTING flag
    -v  Set VERBOSE flag
    -m  Set velocity threshold multiplier (default: 1.0)
    -C  Set Slack channel (default: #kepler_breakouts)
    -H  Set number of hours to include for post_dates (default: 24)
    -N  Set current time (now) string (default: current time, eg "2016-12-31 13:00")
    -M  Set minimum shares for alert (default: 200)
    -P  Set percentile for threshold (default: 90)
    
EOF
}

options () {
    TESTING=""
    VERBOSE=""
    N_HOURS="24"
    VEL_THRESH="1.0"
    CHANNEL="#kepler_breakouts"
    NOW_STR=`date +"%F %H:%M"`
    MIN_SHARES="200"
    PERCENTILE="90"

    while getopts "htvm:C:H:N:M:P:" OPTION
    do
	case $OPTION in
	    m)
		VEL_THRESH=$OPTARG
		;;
	    C)
		CHANNEL=$OPTARG
		;;
	    H)
		N_HOURS=$OPTARG
		;;
	    N)
		NOW_STR=$OPTARG
		;;
	    M)
		MIN_SHARES=$OPTARG
		;;
	    P)
		PERCENTILE=$OPTARG
		;;
	    t)
		TESTING="1"
		;;
	    v)
		VERBOSE="1"
		;;
	    h)
		usage
		exit 1
		;;
	    *)
		usage
		exit 1
		;;
		
	esac
    done
}

main() {
    if [ "$VERBOSE" = "1" ]
    then
	echo VEL_THRESH: $VEL_THRESH
	echo CHANNEL: $CHANNEL
	echo N_HOURS: $N_HOURS
	echo NOW_STR: $NOW_STR
	echo MIN_SHARES: $MIN_SHARES
	echo PERCENTILE: $PERCENTILE
    fi
	
    PYTHON="/home/ubuntu/anaconda/bin/python"
    BINDIR="$KEPLERDIR/bin/velbreak"

    if [ "$TESTING" = "1" ]
    then
	BINDIR="."
    fi

    EXCFIL="$BINDIR/exclusions.csv"

    $PYTHON $BINDIR/kepler_velbreak.py -n "$NOW_STR" -H "$N_HOURS" -P "$PERCENTILE" -M "$MIN_SHARES" -m "$VEL_THRESH" -E "$EXCFIL" -L debug > /tmp/slack_keplerbo.tsv 2> /tmp/slack_keplerbo.log
    if [ -s /tmp/slack_keplerbo.tsv ]
    then
	$PYTHON $BINDIR/slackify.py -f /tmp/slack_keplerbo.tsv -T "$NOW_STR" -C $CHANNEL
    fi
}

options "$@"
main
