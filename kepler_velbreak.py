#
#
#
#    kepler_baseline.py
#
#
#

import os
import sys
import logging

import numpy as np
import pandas as pd
import datetime as dt

from urlparse import urlparse
from csv import QUOTE_NONNUMERIC
from argparse import ArgumentParser
from numpy import log10, percentile, NaN
from scipy.stats import percentileofscore

from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL

# credentials for live_analytics
LA_DB_CREDS = {
    'database': os.getenv('LA_DB_NAME', 'live_analytics'),
    'username': os.getenv('LA_DB_USER', 'spartznet'),
    'password': os.getenv('LA_DB_PASSWORD', '2008DodgeViper600horsepower'),
    'drivername': os.getenv('LA_DB_DRIVER', 'mysql'),
    'host': os.getenv('LA_DB_HOST',
                      'spartzlivereplica-datateam.cjbjovj1ugkv.us-east-1.rds.amazonaws.com'),
}

def la_sqlalch_eng():
    """
    return engine for live_analytics
    """

    return create_engine(URL(**LA_DB_CREDS))


def calc_vel_score(metric_df):
    metric_df = metric_df.reset_index()

    sc_id = metric_df.source_content_id.values[0]
    cr_date = metric_df.cr_date.values[0]
    cur_shares = float(metric_df['count'].values[0])
    vel_per_min, vel_score = NaN, NaN

    if (len(metric_df) > 1 and cur_shares > 0):
        share_values = metric_df['count'].values.astype(float)
        time_values = metric_df.created.values.astype(float)

        share_diff = cur_shares - share_values[1]
        time_diff = time_values[0] - time_values[1]

        vel_per_sec = share_diff/time_diff
        vel_per_min = 60.0*vel_per_sec

        normalizer = log10(cur_shares)
        vel_score = vel_per_min / normalizer

        if (share_diff > 100):
            msg_fmt = "%s %d %d %.3e %.3e %.3e %.3e %d"
            msg = msg_fmt % (sc_id, share_diff, time_diff, vel_per_sec, vel_per_min,
                             vel_score, normalizer, cur_shares)
            logging.debug(msg)

    ret_dict = {'velocity_score': vel_score,
                'velocity_per_min': vel_per_min,
                'highest_count': cur_shares,
                'cr_date': cr_date}

    return pd.DataFrame(ret_dict, index=pd.Index([sc_id], name="source_content_id"))


convert_date_str = lambda s: 'convert_tz("%s","us/central","utc")' % s


def get_sources(engine, asof_date_str, n_hours=24, excluded_domains_str=""):
    """
    get source information for source_group_id=1 and where post_dates
    are withing n_hours of the asof_date_str
    """

    asof_str = convert_date_str(asof_date_str)

    query = """
    select
        s.domain Source,
        sc.id source_content_id,
        sc.url URL,
        sc.title Title,
        sc.source_id,
        sc.post_date post_timestamp,
        convert_tz(from_unixtime(sc.post_date), "utc", "us/central") post_datetime,
        (unix_timestamp({asof_str})/3600.0 - sc.post_date/3600.0) post_age
    from
        (
            select id, url, title, source_id, min(post_date) post_date
            from source_content
            group by title, source_id
        ) sc
    join
        live_analytics.sources s
        on
            s.id = sc.source_id
        and s.active = 1
        and s.source_group_id = 1
    where
        sc.post_date >= (unix_timestamp({asof_str}) - ({n_hours}*60*60))
    and sc.post_date < unix_timestamp({asof_str})
    and sc.url not in (
                        select scrape_source
                        from live_sharedata.lists
                        where created_date < unix_timestamp({asof_str})
                      )
    and s.domain not in ({excluded_domains_str})
    order by
        sc.post_date
    """.format(asof_str=asof_str,
               n_hours=n_hours,
               excluded_domains_str=excluded_domains_str)

    logging.debug("Source Query:\n%s" % query)

    index_col = None
    date_cols = "post_datetime"
    df = pd.read_sql_query(query, engine, parse_dates=date_cols, index_col=index_col)
    return df


def get_metrics(engine, sc_ids, asof_date_str):
    """
    collect data from source_content_metrics for
    specified source_content ids as of a certain date
    """

    sc_ids_str = ",".join(sc_ids)
    asof_str = convert_date_str(asof_date_str)

    query = """
    select
        scm.source_content_id,
        scm.count,
        scm.created,
        convert_tz(from_unixtime(scm.created), "UTC", "US/Central") cr_date
    from
        source_content_metrics scm
    where
        scm.source_content_id in ({sc_ids_str})
    and scm.created < unix_timestamp({asof_str})
    order by
        scm.source_content_id,
        scm.created desc
    """.format(sc_ids_str=sc_ids_str, asof_str=asof_str)

    logging.debug("Metric Query:\n%s" % query)

    index_col = None
    date_cols = "cr_date"
    df = pd.read_sql_query(query, engine, parse_dates=date_cols, index_col=index_col)
    return df


def get_vel_score(metric2_df):
    """
    calculate velocity scores for df
    """
    df = metric2_df.groupby('source_content_id', sort=False, group_keys=False).apply(calc_vel_score)
    return df


def config_logging(level):
    """
    configure logging level
    """

    LEVELS = {
        'debug': logging.DEBUG,
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL
    }
    if (level is not None):
        fmt = "%(asctime)s - %(levelname)s - %(message)s"
        logging.basicConfig(format=fmt,
                            level=LEVELS.get(level, logging.NOTSET))


def parse_args():
    """
    parse command line arguments
    """
    
    desc = "Kepler velocity breakout"
    parser = ArgumentParser(description=desc)

    parser.add_argument('-n', dest='now_str', default=None)
    parser.add_argument('-L', dest='log_level', default=None)
    parser.add_argument('-m', dest='velthresh_multiplier', type=float, default=1.0)
    parser.add_argument('-M', dest='min_shares', type=int, default=200)
    parser.add_argument('-P', dest='percentile', type=float, default=95.0)
    parser.add_argument('-H', dest='hours_ago', type=int, default=24)
    parser.add_argument('-E', dest='exclusions_file', default=None)

    return parser.parse_args()


def main():
    """
    main function
    """

    args = parse_args()
    config_logging(args.log_level)
    logging.debug(args)

    now = dt.datetime.strptime(args.now_str, "%Y-%m-%d %H:%M")
    now_lastweek = now + dt.timedelta(days=-7)
    logging.debug(str(now))
    logging.debug(str(now_lastweek))

    sqlalch_engine = la_sqlalch_eng()

    excluded_domains_str = '""'
    if (args.exclusions_file is not None):
        exclusions_df = pd.read_csv(args.exclusions_file)
        excluded_domains_str = ",".join("'%s'"%d.lower() for d in exclusions_df.exclusions)
        logging.debug("excluded_domains_str: \n%s" % excluded_domains_str)

    logging.info("Getting today's sources")
    today_sources = get_sources(sqlalch_engine, args.now_str, args.hours_ago, excluded_domains_str)
    logging.info("Getting last week's sources")
    lastweek_sources = get_sources(sqlalch_engine, str(now_lastweek), excluded_domains_str=excluded_domains_str)

    logging.info("Getting today's metrics")
    today_source_ids = list(today_sources.source_content_id.astype(str))
    today_metrics = get_metrics(sqlalch_engine, today_source_ids, args.now_str)

    logging.info("Getting last week's metrics")
    lastweek_source_ids = list(lastweek_sources.source_content_id.astype(str))
    lastweek_metrics = get_metrics(sqlalch_engine, lastweek_source_ids, str(now_lastweek))

    logging.info("Calculating last week's score distribution")
    rows = (lastweek_metrics.cr_date <= str(now_lastweek))
    lw_latest_2 = lastweek_metrics[rows].groupby("source_content_id").head(2)
    lw_vel_df = get_vel_score(lw_latest_2)

    logging.info("Calculating current & previous scores")
    rows = (today_metrics.cr_date <= args.now_str)
    latest_3 = today_metrics[rows].groupby("source_content_id").head(3)
    latest_2 = latest_3.groupby("source_content_id").head(2)
    previous_2 = latest_3.groupby("source_content_id").tail(2)

    vel_df = get_vel_score(latest_2).dropna()
    prev_vel_df = get_vel_score(previous_2).dropna()
    joined = vel_df[vel_df.velocity_score > 0].join(prev_vel_df, rsuffix='_p', how='left')
    joined = joined[joined.cr_date != joined.cr_date_p]

    logging.info("Calculating %.0f percentile value of last week's score distribution" % args.percentile)
    rows = (lw_vel_df.velocity_score > 0)
    dist_scores = lw_vel_df[rows].velocity_score
    vel_thresh = np.percentile(dist_scores, args.percentile)

    shares_thresh = args.min_shares
    rows = ((joined.velocity_score >= vel_thresh) &
            (joined.velocity_score_p < (vel_thresh * args.velthresh_multiplier)) &
            (joined.highest_count >= shares_thresh))

    if (sum(rows) > 0):
        cols = ["highest_count",
                "velocity_per_min",
                "velocity_score",
                "Score Percentile",
                "Source",
                "URL",
                "Title",
                "post_datetime",]
        joined = joined[rows].join(today_sources.set_index("source_content_id"))
        joined['URL'] = joined.URL.str.decode("cp1252").str.encode("utf8")
        joined['Title'] = joined.Title.str.decode("cp1252").str.encode("utf8")
        joined['Score Percentile'] = joined.velocity_score.apply(lambda s: percentileofscore(dist_scores.values, s, kind='strict'))
        print joined[cols].sort_values("velocity_score", ascending=False).to_csv(sep="\t", quoting=QUOTE_NONNUMERIC, encoding='utf8')


if (__name__ == '__main__'):
    main()
