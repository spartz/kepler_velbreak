#!/usr/bin/env python
import json
import argparse
import requests
import delorean
import pandas as pd

#INTEGRATION_URL = "https://hooks.slack.com/services/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"
INTEGRATION_URL = "https://hooks.slack.com/services/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"


def fields(row):
    cols = [
        ("Domain", "Source"),
        ("Posted", "humanized"),
        ("Shares", "highest_count"),
        ("Shares/Min", "velocity_per_min"),
        ("Score", "velocity_score"),
        ("Score Percentile", "Score Percentile"),
    ]
    return [{"title": title, "value": row[col], "short": True} for title, col in cols]


def attachment(row):
    out_ = {
        "title": row['Title'],
        "fallback": row['Title'],
        "title_link": row['URL'],
        "color": "good",
        "fields": fields(row),
    }
    return out_


def parse_arguments():
    parser = argparse.ArgumentParser(description='Convert Kepler output into slack messages')
    parser.add_argument('-f', '--file', help="Input file (i.e. kepler output).")
    #parser.add_argument('-t', '--threshold', help="Filter out rows with Multiple score below threshold.")
    parser.add_argument('-C', '--channel', help="Alternate Slack channel", default=None)
    parser.add_argument('-T', '--timestamp', help="Timestamp for Slack message")
    return parser.parse_args()


def main():
    args = parse_arguments()
    df = pd.read_csv(args.file, sep="\t")

    rounder = lambda v: round(v, 1)
    df['velocity_score'] = df.velocity_score.apply(rounder)
    df['velocity_per_min'] = df.velocity_per_min.apply(rounder)
    df['Score Percentile'] = df['Score Percentile'].apply(rounder)

    humanize = lambda d: delorean.parse(d, timezone="US/Central", dayfirst=False).humanize()
    df["humanized"] = df.post_datetime.apply(humanize)

    attachments_ = []
    for idx, row in df.iterrows():
        attachments_.append(attachment(row))

    out_ = {'text': 'Kepler Velocity Breakout {}'.format(args.timestamp),
            'username': 'KeplerBot',
            'attachments': attachments_}

    if (args.channel is not None):
        out_['channel'] = args.channel

    json_out_ = json.dumps(out_, indent=1)
    print(json_out_)

    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(INTEGRATION_URL, data=json_out_, headers=headers)
    r.raise_for_status()


if __name__ == '__main__':
    main()



